package gee.yuu.practiceapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.view.View
import android.widget.Button
import gee.yuu.practiceapp.fragments.DisplayTextFragment
import gee.yuu.practiceapp.fragments.EnterTextFragment
import gee.yuu.practiceapp.R
import gee.yuu.practiceapp.services.RetroFitPractice
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_display_text.*


class MainActivity : AppCompatActivity(), EnterTextFragment.OnFragmentInteractionListener {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeFragments()
        Log.d("DEBUG", "In onCreate----------------------------------------------------------------------")
        handleRestCall()
        val numbers = arrayOf(1, 2, 3, 4, 5,6)
        Log.d("DEBUG", "List of numbers is: ${numbers.size}")  //All 6 numbers
        val mappedNumbers = numbers.filter { it -> it % 2 == 0 }
        Log.d("DEBUG", "List of mapped numbers is: ${mappedNumbers.size}") //Only 3 numbers
    }


    fun initializeFragments(){
        val ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(enterTextFragmentHolder.id, EnterTextFragment(), "enterTextFragmentHolder")
        ft?.replace(displayTextFragmentHolder.id, DisplayTextFragment(), "displayTextFragmentHolder")
        ft?.commit()
    }

    fun handleRestCall(){
        val myJSONService = RetroFitPractice().createService("https://my-json-server.typicode.com/")
        val jsonObservable = myJSONService.getProfile()
        jsonObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = { Log.d("DEBUG", " ${it.profileName}")
                                    updatedTextFromButton.text = it.profileName},
                        onError = { Log.d("DEBUG","ERRORS MAN") }

                )
        Log.d("DEBUG", "----------------------------------------------------------------------")
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    override fun onFragmentButtonPress(text: String) {
        updatedTextFromButton.text = "Used an interface to get: ${text}!"
    }

    fun determineButtonLogic(view: View){
        if(view == showEnterTextFragment){
            showFragment(supportFragmentManager.findFragmentByTag("enterTextFragmentHolder"), resources.getString(R.string.show_enter_text_fragment_button_text), resources.getString(R.string.hide_enter_text_fragment_button_text), showEnterTextFragment)
        }
        if(view == showDisplayTextFragment){
            showFragment(supportFragmentManager.findFragmentByTag("displayTextFragmentHolder"),resources.getString(R.string.show_display_text_fragment_button_text), resources.getString(R.string.hide_display_text_fragment_button_text), showDisplayTextFragment)
        }
        if(view == toWikiActivity){
            intent = Intent(this, WikiActivity::class.java)
            intent.putExtra("textFromMainActivity", "Hello. You just came from the main activity.")
            startActivity(intent)
        }
        if(view == toDogeActivity){
            intent = Intent(this, DogeActivity::class.java)
            startActivity(intent)
        }
    }

    fun showFragment(fragment: Fragment, show: String, hide: String, button: Button){
        fragment.view?.visibility = if(fragment.view?.visibility == View.GONE){
            button.text = hide
            View.VISIBLE
        } else{
            button.text = show
            View.GONE
        }
    }

}
