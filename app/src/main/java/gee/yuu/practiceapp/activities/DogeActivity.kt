package gee.yuu.practiceapp.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity;
import android.view.View
import gee.yuu.practiceapp.data.DogeAdapter
import gee.yuu.practiceapp.fragments.MyRecyclerViewFragment
import gee.yuu.practiceapp.R
import kotlinx.android.synthetic.main.activity_doge.*


class DogeActivity : AppCompatActivity(), MyRecyclerViewFragment.OnFragmentInteractionListener {

    val dogeFragment = MyRecyclerViewFragment()

    override fun updateRecyclerView(adapter: DogeAdapter, position: Int) {
        runOnUiThread {adapter.notifyItemInserted(position)}

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doge)

        val ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        if(savedInstanceState != null){
            ft?.replace(fragmentRecyclerViewHolder.id, supportFragmentManager.getFragment(savedInstanceState, "dogeFragment"), "dogeFragmentHolder")
        }
        else{
            ft?.replace(fragmentRecyclerViewHolder.id, dogeFragment, "dogeFragmentHolder")
        }
        ft?.commit()
    }

    fun toMainActivity(view: View){
        intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        supportFragmentManager.putFragment(outState, "dogeFragment", supportFragmentManager.findFragmentByTag("dogeFragmentHolder"))
    }
}
