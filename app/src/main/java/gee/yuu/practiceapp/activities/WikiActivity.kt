package gee.yuu.practiceapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.util.Log
import android.view.View
import gee.yuu.practiceapp.R
import gee.yuu.practiceapp.services.WikiSearchService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_wiki.*
import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences



class WikiActivity : AppCompatActivity() {
    val wikiSearchService by lazy { WikiSearchService.create() }
    var disposable: Disposable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wiki)
        messageFromIntent.text = intent.getStringExtra("textFromMainActivity")
        Log.d("DEBUG", "-----------------onCreate savedInstanceState is $savedInstanceState")
        val pref = applicationContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE)

        resultsDisplay.text = pref.getString("resultsDisplay", "Tapping the button will return the total hit count of the query provided. This is possible using Wiki's open query API.")
        searchField.setText(pref.getString("searchField", "Enter anything"))
    }

    fun search(view: View){
        Log.d("DEBUG", "Value of search query is: ${searchField.text.toString()}")
        disposable = wikiSearchService.getHitCount("query", "json", "search", searchField.text.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> resultsDisplay.text = "Total hit count: ${result.query.searchInfo.totalHit}"},
                        { error -> resultsDisplay.text = "$error"}
                )
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
        val pref = applicationContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString("resultsDisplay", resultsDisplay.text as String?)
        editor.putString("searchField", searchField.text.toString())
        editor.apply()
    }

    override fun onDestroy() {
        super.onDestroy()
        val pref = applicationContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.clear()
        editor.apply()
    }

    fun toMainActivity(view: View){
        intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString("resultsDisplay", resultsDisplay.text as String?)
        outState?.putString("searchField", searchField.text.toString())
        Log.d("DEBUG", "-----------------Just saved instance $outState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.d("DEBUG", "-----------------onRestoreInstanceState savedInstanceState is $savedInstanceState")
        savedInstanceState?.let {
            resultsDisplay.text = it.getString("resultsDisplay")
            searchField.setText(it.getString("searchField"))
        }
    }
}
