package gee.yuu.practiceapp.activities

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gee.yuu.practiceapp.R
import gee.yuu.practiceapp.data.Doge

class TestAdapter : RecyclerView.Adapter<TestHolder>() {
    var items = emptyList<Doge>()
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestHolder {
        return TestHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_recycler_view_list_row, parent, false))
    }

    override fun onBindViewHolder(holder: TestHolder, position: Int) {
        holder.bind(items[position])
    }

    fun addItems(newItem: Doge) {
        // TODO:: Implement data modifiers
        // TODO:: Avoid notifyDataSetChanged()
    }
}

class TestHolder(view: View) : RecyclerView.ViewHolder(view) {
    // val textView = view.textView

    fun bind(item: Doge) {
        // textView.text = item.propery.toString()
    }
}