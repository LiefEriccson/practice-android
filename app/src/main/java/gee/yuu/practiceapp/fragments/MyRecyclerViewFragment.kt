package gee.yuu.practiceapp.fragments

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gee.yuu.practiceapp.R
import gee.yuu.practiceapp.data.Doge
import gee.yuu.practiceapp.data.DogeAdapter
import kotlinx.android.synthetic.main.fragment_my_recycler_view.*
import java.util.*
import kotlin.concurrent.schedule

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MyRecyclerView.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MyRecyclerView.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MyRecyclerViewFragment : Fragment(){
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var loopCount = 1
    private var myAdapter = DogeAdapter()

    companion object {
        @JvmStatic private var recyclerViewState: Bundle? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("DEBUG", "onViewCreated within MyRecyclerViewFragment. Value of myAdapter is: ${myAdapter}")

        if(myRecyclerView.adapter == null){
            myRecyclerView.adapter = myAdapter
        }
        if(myRecyclerView.layoutManager == null){
            myRecyclerView.layoutManager = LinearLayoutManager(this.context)
        }

    }

    override fun onResume() {
        super.onResume()
        if(recyclerViewState != null){
            val state: Parcelable? = recyclerViewState?.getParcelable("recycledState")
            myRecyclerView.layoutManager.onRestoreInstanceState(state)
        }
        loopAddItems()
    }

    override fun onPause() {
        super.onPause()
        recyclerViewState = Bundle()
        val state = myRecyclerView.layoutManager.onSaveInstanceState()
        recyclerViewState?.putParcelable("recycledState", state)
    }

//    override fun onSaveInstanceState(state: Bundle) {
//        super.onSaveInstanceState(state)
//
//        // Save list state
//        val mListState = myRecyclerView.layoutManager.onSaveInstanceState()
//        state.putParcelable("Doge Position", mListState)
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//
//        // Retrieve list state and list/item positions
//        if (savedInstanceState != null){
//            val mListState: Parcelable = savedInstanceState.getParcelable("Doge Position")
//            myRecyclerView.layoutManager.onRestoreInstanceState(mListState)
//        }
//    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun loopAddItems(){
        Timer().schedule(500){
            Log.d("DEBUG", "Inside loopAddItems(). Looped $loopCount times")
            myAdapter.items.add(Doge(loopCount++))
            val newItemPosition = myAdapter.items.size - 1
            listener?.updateRecyclerView(myAdapter, newItemPosition)
            loopAddItems()
        }
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun updateRecyclerView(adapter: DogeAdapter, position: Int)
    }


}
