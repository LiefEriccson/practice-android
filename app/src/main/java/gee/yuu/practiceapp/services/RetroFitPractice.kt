package gee.yuu.practiceapp.services

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetroFitPractice{

    fun createService(baseURL: String): MyJSONDataCallService {
        val retroFit = retrofit2.Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(MyJSONDataCallService::class.java)
        return retroFit
    }

}