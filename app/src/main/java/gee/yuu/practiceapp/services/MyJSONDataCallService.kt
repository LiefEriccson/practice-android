package gee.yuu.practiceapp.services

import gee.yuu.practiceapp.data.APIResponse
import io.reactivex.Single
import retrofit2.http.GET

interface MyJSONDataCallService {

    @GET("typicode/demo/profile")
    fun getProfile(): Single<APIResponse>

    @GET("jokes/random")
    fun randomJoke(): Single<APIResponse>
}