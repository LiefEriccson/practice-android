package gee.yuu.practiceapp.data

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import gee.yuu.practiceapp.R
import kotlinx.android.synthetic.main.my_recycler_view_list_row.view.*

class DogeAdapter : RecyclerView.Adapter<MyViewHolder>(){
    val items = mutableListOf<Doge>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_recycler_view_list_row, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.addToList(items[position])
    }

}

class MyViewHolder(view: View):RecyclerView.ViewHolder(view) {
    val titleText = view.titleText
    val descriptionText = view.descriptionText
    var dogeImage = view.imageFromWeb

    fun addToList(doge: Doge){
        titleText.text = doge.name
        descriptionText.text = doge.number
        dogeImage.setImageResource(doge.dogeImage)
    }
}
