package gee.yuu.practiceapp.data

import gee.yuu.practiceapp.R

data class Doge(val dogeNumber: Int){
    val name: String = "Doge"
    val number: String = "I am Doge #$dogeNumber"
    val dogeImage: Int = R.mipmap.doge
}
