package gee.yuu.practiceapp.data

import com.google.gson.annotations.SerializedName

data class APIResponse (
        @SerializedName("name") val profileName: String,
        @SerializedName("type") val type: String,
        @SerializedName("value") val joke: Joke
)