package gee.yuu.practiceapp.data

import com.google.gson.annotations.SerializedName

object WikiModel {
    data class Result(@SerializedName("query") val query: Query)
    data class Query(@SerializedName("searchinfo") val searchInfo: SearchInfo)
    data class SearchInfo(@SerializedName("totalhits") val totalHit: Int)
}